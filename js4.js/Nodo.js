class Nodo {
    constructor(tipo,nivel,posicion,valor,HijoIz:number=0,HijoDe:number=0 ) {
        this.tipo= tipo;
        this. valor= valor;
        this.nivel= nivel;
        this.HijoIz=HijoIz;
        this.HijoDe=null;
        this.posicion=posicion;

        if (HijoIz=1)
            this.hijoIzquierda= this.crearHijo (tipo, "i", valor,"0",nivel,+1,this.nivel )
        if (HijoDe=1)
            this.hijoDerecha= this.crearHijo (tipo, "i", valor,"0",nivel,+1,this.nivel )
    }
    crearHijo (tipo,valor,nivel,padre){
        var crearHijo= new Nodo(tipo,valor,nivel,padre);
        return crearHijo;
    }

}