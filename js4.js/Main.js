var arbol = new Arbol();

arbol.NodoPadre.HijoIz= arbol.agregarNodo(arbol.NodoPadre, "HOJA IZQUIERDA");
arbol.NodoPadre.HijoDe= arbol.agregarNodo(arbol.NodoPadre, "HOJA DERECHA");

arbol.NodoPadre.HijoIz.HijoIz= arbol.agregarNodo(arbol.NodoPadre, "HOJA IZQUIERDA");
arbol.NodoPadre.HijoIz.HijoDe= arbol.agregarNodo(arbol.NodoPadre, "HOJA DERECHA");

arbol.NodoPadre.HijoIz.HijoDe.HijoIz= arbol.agregarNodo(arbol.NodoPadre, "HOJA IZQUIERDA");
arbol.NodoPadre.HijoIz.HijoDe.HijoDe= arbol.agregarNodo(arbol.NodoPadre, "HOJA DERECHA");

console.log(arbol);